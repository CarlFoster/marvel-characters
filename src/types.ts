
interface Thumbnail {
  path: string;
  extension: "jpg" | "png";
}

interface ResultSet<T> {
  available: number
  returned: number
  collectionURI: string
  items: Array<T>
}

export interface Paging<T> {
  count: number
  limit: number
  offset: number
  total: number
  results: T[]
}

export interface Summary {
  resourceURI: string
  name: string
}

interface StorySummary extends Summary {
  type: 'interior' | 'cover'
}

export interface CharacterDetails {
  id: number;
  description: string;
  name: string;
  thumbnail: Thumbnail;
  resourceURI: string;
  comics: ResultSet<Summary>
  stories: ResultSet<StorySummary>
  events: ResultSet<Summary>
  series: ResultSet<Summary>
}